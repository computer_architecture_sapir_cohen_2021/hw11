#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <iostream>
#include <ctime>

#define SIZE 5
#define MAX 1000000
#define ASCII '0'

HANDLE forks[SIZE]; //mutex
LPCWSTR names[SIZE] = { L"0", L"1", L"2", L"3", L"4" }; //names for each mutex

/* Enter full path of exe file here: */
wchar_t argEat[] = L"E:\\Sapir\\magshimim-windows\\sapir_cohen_hw11\\enter_TP_Sapir_for_sln-(HW)\\TP_Sapir\\Debug\\eat_process.exe  "; // path of exe file (2 spaces for place to argument)

int main()
{
	time_t begintime, endtime;

	PROCESS_INFORMATION process_info[SIZE] = { 0 };
	STARTUPINFO process_startup_info[SIZE] = { 0 };

	for (int i = 0; i < SIZE; i++)
	{
		ZeroMemory(&process_startup_info[i], sizeof(process_startup_info[i]));
		process_startup_info[i].cb = sizeof(process_startup_info[i]);
		ZeroMemory(&process_info[i], sizeof(process_info[i]));
	}
	
	//init for mutexes
	for (int i = 0; i < SIZE; i++)
	{
		forks[i] = CreateMutex(NULL,
			FALSE,
			names[i]);

		if (forks[i] == NULL)
		{
			std::cout << "CreateMutex error: " << GetLastError() << std::endl;
			return 1;
		}
	}

	time(&begintime);

	//Philosophers processes
	for (int i = 0; i < SIZE; i++)
	{
		argEat[wcslen(argEat) - 1] = char(i  + ASCII);
		if (!CreateProcess(NULL,
			argEat,  // Command line
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			NULL,
			&process_startup_info[i],
			&process_info[i]))
		{
			std::cout << "process " << i << " failed" << std::endl;
			return 1;
		}
	}

	for (int i = 0; i < SIZE; i++)
	{
		WaitForSingleObject(process_info[i].hProcess, INFINITE);
		CloseHandle(process_info[i].hProcess);
		CloseHandle(process_info[i].hThread);
	}

	time(&endtime);
	std::cout << "Time: " << endtime - begintime << std::endl;

	for (int i = 0; i < SIZE; i++) CloseHandle(&forks[i]);

	return 0;
}