#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <iostream>
#include <ctime>

#define SIZE 5
#define MAX 1000000

CRITICAL_SECTION sticks[SIZE]; //critical section
INT counter[SIZE] = { 0 };

DWORD WINAPI eat_with_chopsticks(LPVOID param)
{
	while (counter[*(int*)(param)] != MAX) //checking if he ate MAX times
	{
		if (TryEnterCriticalSection(&sticks[*(INT*)(param)]))
		{
			if (TryEnterCriticalSection(&sticks[(*(INT*)(param)+1) % SIZE]))
			{

				std::cout << *(INT*)(param)+1 << std::endl;
				counter[*(INT*)(param)]++;

				LeaveCriticalSection(&sticks[(*(INT*)(param)+1) % SIZE]);
				LeaveCriticalSection(&sticks[*(INT*)(param)]);
			}
			else LeaveCriticalSection(&sticks[*(INT*)(param)]);
		}
	}
	return 0;
}

int main_chopsticks()
{
	HANDLE WINAPI threads[SIZE];
	time_t begintime, endtime;

	int arr[SIZE] = { 0, 1, 2, 3, 4 };

	//init for CS
	for (int i = 0; i < SIZE; i++) InitializeCriticalSection(&sticks[i]);

	time(&begintime);

	//Philosophers threads
	for (int i = 0; i < SIZE; i++)
	{
		threads[i] = CreateThread(
			NULL,
			0,
			eat_with_chopsticks,
			arr + i,
			0,
			NULL);

		if (threads[i] == NULL)
		{
			std::cout << "Could not create thread" << std::endl;
			return 1;
		}

	}

	for (int i = 0; i < SIZE; i++)
	{
		WaitForSingleObject(threads[i], INFINITE);
		CloseHandle(&threads[i]);
	}
	
	time(&endtime);
	std::cout << "Time: " << endtime - begintime << std::endl;

	for (int i = 0; i < SIZE; i++) DeleteCriticalSection(&sticks[i]);

	return 0;
}
