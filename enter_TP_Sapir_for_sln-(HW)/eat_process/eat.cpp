#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <iostream>
#include <string>

#define MAX_P 5
#define SIZE 2
#define MAX 1000000
#define MIN_CMD 2
#define ASCII '0'

HANDLE forks[SIZE]; //mutex available
int counter = 0; //counter for how many times he ate

int main(int argc, char* argv[])
{
	//The names of the mutexes available to this philosopher
	std::wstring names[SIZE] = { std::to_wstring(*argv[1] - ASCII) ,
								std::to_wstring((*argv[1] - ASCII + 1) % MAX_P) };

	if (argc != MIN_CMD)
	{
		std::cout << "Usage: " << argv[0] << " [cmdline]" << std::endl;
		return 1;
	}

	//mutex available to this philosopher creating
	for (int i = 0; i < SIZE; i++)
	{
		forks[i] = CreateMutex(NULL,
			FALSE,
			names[i].c_str());

		if (forks[i] == NULL)
		{
			std::cout << "CreateMutex error: " << GetLastError() << std::endl;
			return 1;
		}
	}

	while (counter != MAX) //checking if he ate MAX times
	{
		if (WaitForSingleObject(forks[0], 1) != WAIT_TIMEOUT)
		{
			if (WaitForSingleObject(forks[1], 1) != WAIT_TIMEOUT)
			{
				std::cout << *argv[1] - ASCII + 1 << std::endl; /* printing who eats */
				counter++;
				ReleaseMutex(forks[1]);
				ReleaseMutex(forks[0]);
			}
			else ReleaseMutex(forks[0]);
		}
	}

	for (int i = 0; i < SIZE; i++)
	{
		CloseHandle(&forks[i]);
	}

	return 0;
}
