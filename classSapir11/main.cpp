#include <Windows.h>
#include <iostream>

#define SIZE 4

DWORD WINAPI thread_main(LPVOID param)
{
	for (int i = 0; i < 100; i++)
	{
		std::cout << *(int*)param;
		Sleep(50);
	}
	std::cout << std::endl;
	return 0;
}

int main()
{
	int id[SIZE] = { 1, 2, 3, 4 };
	HANDLE WINAPI hThreads[SIZE] = { 0 };
	for (int i = 0; i < SIZE; i++)
	{
		hThreads[i] = CreateThread(
			NULL,
			0,
			thread_main,
			&id[i],
			0,
			NULL);
	}
	for (int i = 0; i < SIZE; i++) WaitForSingleObject(hThreads[i], INFINITE);
	return 0;
}